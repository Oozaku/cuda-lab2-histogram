#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>


#define COMMENT "Histogram_GPU"
#define RGB_COMPONENT_COLOR 255

typedef struct {
	unsigned char red, green, blue;
} PPMPixel;

typedef struct {
	int x, y;
	PPMPixel *data;
} PPMImage;

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}


static PPMImage *readPPM(const char *filename) {
	char buff[16];
	PPMImage *img;
	FILE *fp;
	int c, rgb_comp_color;
	fp = fopen(filename, "rb");
	if (!fp) {
		fprintf(stderr, "Unable to open file '%s'\n", filename);
		exit(1);
	}

	if (!fgets(buff, sizeof(buff), fp)) {
		perror(filename);
		exit(1);
	}

	if (buff[0] != 'P' || buff[1] != '6') {
		fprintf(stderr, "Invalid image format (must be 'P6')\n");
		exit(1);
	}

	img = (PPMImage *) malloc(sizeof(PPMImage));
	if (!img) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}

	c = getc(fp);
	while (c == '#') {
		while (getc(fp) != '\n')
			;
		c = getc(fp);
	}

	ungetc(c, fp);
	if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
		fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
		exit(1);
	}

	if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
		fprintf(stderr, "Invalid rgb component (error loading '%s')\n",
				filename);
		exit(1);
	}

	if (rgb_comp_color != RGB_COMPONENT_COLOR) {
		fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
		exit(1);
	}

	while (fgetc(fp) != '\n')
		;
	img->data = (PPMPixel*) malloc(img->x * img->y * sizeof(PPMPixel));

	if (!img) {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(1);
	}

	if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
		fprintf(stderr, "Error loading image '%s'\n", filename);
		exit(1);
	}

	fclose(fp);
	return img;
}

__global__ void calc(PPMImage *image, float *h){
    
    int i,j,k,l,count=0,position=blockIdx.x;
    
    /* Calculating values to j,k,l */
    i = position;
    l = i % 4; i = i / 4;
    k = i % 4; i = i / 4;
    j = i;
    
    /* Image's length*/
    float n = image->x * image->y;

    /* Calculating value to h[position] */
    for (i=0;i<n;i++){
        if (image->data[i].red == j && image->data[i].green == k && image->data[i].blue == l)
            count++;
    }
    h[position] = (float) count / n;
}

void Histogram(PPMImage *image, float *h) {

	int i;

	float n = image->y * image->x;

	//printf("%d, %d\n", rows, cols );

	for (i = 0; i < n; i++) {
		image->data[i].red = floor((image->data[i].red * 4) / 256);
		image->data[i].blue = floor((image->data[i].blue * 4) / 256);
		image->data[i].green = floor((image->data[i].green * 4) / 256);
	}

    /*---------------------------CUDA---------------------------*/
	PPMImage *dimage,
             *himage = (PPMImage*) malloc(sizeof(PPMImage));
    float *dh;
    PPMPixel *ddata;
    

    /* Alloc space to struct Image, PPMPixel array, dh array */
    cudaMalloc((void**)&dimage,sizeof(PPMImage));
    cudaMalloc((void**)&ddata,n*sizeof(PPMPixel));
    cudaMalloc((void**)&dh,sizeof(float)*64); 

    /* Copy image struct to device */
    cudaMemcpy(ddata,image->data,n*sizeof(PPMPixel),cudaMemcpyHostToDevice);
    himage->data = ddata;
    himage->x = image->x; himage->y = image->y;
    cudaMemcpy(dimage,himage,sizeof(PPMImage),cudaMemcpyHostToDevice);
    
    /* Copy h to device*/
    cudaMemcpy(dh,h,sizeof(float)*64,cudaMemcpyHostToDevice);

    /* Fill dh array */
    calc<<<64,1>>>(dimage,dh);

    /* Returning h array */
    cudaMemcpy(h,dh,64*sizeof(float),cudaMemcpyDeviceToHost);
    /*----------------------------------------------------------*/

}

int main(int argc, char *argv[]) {

	if( argc != 2 ) {
		printf("Too many or no one arguments supplied.\n");
	}

	double t_start, t_end;
	int i;
	char *filename = argv[1]; //Recebendo o arquivo!;
	
	//scanf("%s", filename);
	PPMImage *image = readPPM(filename);

	float *h = (float*)malloc(sizeof(float) * 64);

	//Inicializar h
	for(i=0; i < 64; i++) h[i] = 0.0;

	t_start = rtclock();
	Histogram(image, h);
	t_end = rtclock();

	for (i = 0; i < 64; i++){
		printf("%0.3f ", h[i]);
	}
	printf("\n");
	fprintf(stdout, "\n%0.6lfs\n", t_end - t_start);  
	free(h);
}
